Health = {}
Health.__index = Health

function Health.create(initial, x, y, person)
	local hp = {}
	setmetatable(hp, Health)
	hp.current = initial
	hp.initial = initial
	hp.x = x
	hp.y = y
	hp.person = person
	return hp
end

function Health:change(amount)
	self.current = self.current + amount
end

function Health:drawBar()
	self.percent = self.current / self.initial
	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle("fill", self.x, self.y - 20, 64, 10)
	love.graphics.setColor(255, 0, 0)
	love.graphics.rectangle("fill", self.x + 2, self.y - 18, 60, 6)
	if self.percent > 0 then
		if (self.person.person == "player") then
			love.graphics.setColor(0, 255, 0)
		elseif (self.person.person == "party") then
			love.graphics.setColor(0, 0, 255)
		elseif (self.person.person == "enemy") then
			love.graphics.setColor(255, 200, 0)
		end
		love.graphics.rectangle("fill", self.x + 2, self.y - 18, self.percent * 60, 6)
	end
end

function Health:setCoordinates(newX, newY)
	self.x = newX
	self.y = newY
end