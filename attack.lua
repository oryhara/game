

Attack = {}
Attack.__index = Attack

function Attack.create(animation, person, offense)
	local att = {}
	setmetatable(att, Attack)
	att.current = "none"
	att.step = 0
	att.x = 0
	att.y = 0
	att.xSpeed = 0
	att.ySpeed = 0
	att.d = 0
	att.length = 0
	att.offense = offense
	att.person = person
	att.target = nil
	att.hitbox = Hitbox.create(5, 5, 50, 50, 5, person)
	att.anim = animation
	return att
end

function Attack:update(newX, newY, newD, newTarget)
	--print (self.xSpeed)
	self.x = newX
	self.y = newY
	self.target = newTarget
	self.d = newD
	if (self.current == "01") then
		self.length = 20
		self:attack01()
	elseif (self.current == "02") then
		self.length = 32
		self:attack02()
	elseif (self.current == "03") then
		self.length = 32
		self:attack03()
	elseif (self.current == "11") then
		self.length = 30
		self:attack11()
	elseif (self.current == "12") then
		self.length = 30
		self:attack12()
	elseif (self.current == "21") then
		self.length = 20
		self:attack21()
	end
	
	if not (self.current == "none") and (self.step < self.length - 1) then
		self.step = self.step + 1
	elseif not (self.current == "none") and (self.step > self.length - 2) then
		self.step = 0
		self.current = "none"
	end
end

function Attack:set(new)
	self.current = new
end

function Attack:check()
	return self.current
end

function Attack:animate()
	if (self.current == "01") then
		self:anim01()
	elseif (self.current == "02") then
		self:anim02()
	elseif (self.current == "03") then
		self:anim03()
	elseif (self.current == "11") then
		self:anim11()
	elseif (self.current == "12") then
		self:anim12()
	elseif (self.current == "21") then
		self:anim21()
	end
end

function Attack:dealDamage(target, stun, effect)
	if self.hitbox:check(target.hurtboxX, target.hurtboxY, target.hurtboxX2, target.hurtboxY2) then
		target.health:change(-self.hitbox.damage - self.offense + target.defense)
		target.hitstun = math.max(stun - target.balance, 0)
		target:setTargetCharacter(self.person)
		
		if (effect == "paralyze") then
			target.hitstun = target.hitstun + 300
			target.paralyze = true
		end
		if (effect == "poison") and (math.random(5) < 5) then
			target.poison = 1000
		end
		
		self.hitbox.hit = false
	end
end

function Attack:checkDamage(stun, effect)
	if (self.person.person == "enemy") then
		for i in pairs(characters) do
			if (characters[i].person == "party") or (characters[i].person == "player") and not (characters[i].isDead) then
				self:dealDamage(characters[i], stun, effect)
			end
		end
		
	elseif (self.person.person == "party") or (self.person.person == "player") then
		for i in pairs(characters) do
			if (characters[i].person == "enemy") and not (characters[i].isDead) then
				self:dealDamage(characters[i], stun, effect)
			end
		end
	end
end


--standard stab
function Attack:attack01(dt)
	if (self.step == 13) then
		self.hitbox:standard0()
	
		self:checkDamage(10)
	end
end

--charge
function Attack:attack02(dt)
	if self.step == 13 then
		self.xSpeed = 420 * self.target:xUnit()
		self.ySpeed = 420 * self.target:yUnit()
	end
	if (self.step == 28) then
		self.hitbox:standard0()
		
		self.xSpeed = 0
		self.ySpeed = 0
		self:checkDamage(10, "paralyze")
	end
end

--defensive poke
function Attack:attack03(dt)
	if (self.step == 13) then
		self.hitbox:standard0()
	
		self:checkDamage(10, "poison")
	end
	if self.step == 28 then
		print (self.target:xUnit())
		print (self.target:yUnit())
		self.xSpeed = -420 * self.target:xUnit()
		self.ySpeed = -420 * self.target:yUnit()
	end
	if self.step == 31 then
		self.xSpeed = 0
		self.ySpeed = 0
	end
end

--standard swipe
function Attack:attack11(dt)
	if (self.step == 20) then 
		self.hitbox:standard1()
	
		self:checkDamage(15)
	end
end

--step swipe
function Attack:attack12(dt)
	
	if self.step == 1 then
		if self.d == 0 then
			self.xSpeed = -420
		elseif self.d == 1 then
			self.ySpeed = 420
		elseif self.d == 2 then
			self.xSpeed = 420
		else
			self.ySpeed = -420
		end
	end
	
	if self.step == 5 then
		self.xSpeed = 0
		self.ySpeed = 0
	end
	if self.step == 12 then
		if self.d == 0 then
			self.xSpeed = 420
		elseif self.d == 1 then
			self.ySpeed = -420
		elseif self.d == 2 then
			self.xSpeed = -420
		else
			self.ySpeed = 420
		end
	end
	if (self.step == 20) then 
		self.hitbox:standard1()
	
		self:checkDamage(15)
		self.xSpeed = 0
		self.ySpeed = 0
	end
end

--ranged attack
function Attack:attack21(dt)
	if (self.step == 13) then 
		self.person.movingAttack:initialize()
	end
end

--ranged attack with knockback
function Attack:attack21(dt)
	if (self.step == 13) then 
		self.person.movingAttack:initialize()
	end
end

function Attack:anim01()
	for i=0, 3 do
		if self.d == i and self.step < 5 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 9 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 13 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 20 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end

function Attack:anim02()
	for i=0, 3 do
		if self.d == i and self.step < 14 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 20 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 28 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 32 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end

function Attack:anim03()
	for i=0, 3 do
		if self.d == i and self.step < 14 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 20 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 28 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 32 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end

function Attack:anim11()
	for i=0, 3 do
		if self.d == i and self.step < 12 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[1][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 16 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[1][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 20 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[1][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 30 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[1][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end

function Attack:anim12()
	for i=0, 3 do
		if self.d == i and self.step < 12 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[1][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 16 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[1][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 20 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[1][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 30 then
			if self.d == 0 then
				love.graphics.draw(sword[1][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[1][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end

function Attack:anim21()
	for i=0, 3 do
		if self.d == i and self.step < 5 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][1], self.x, self.y)
				love.graphics.draw(sword[0][i][1], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 9 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][2], self.x, self.y)
				love.graphics.draw(sword[0][i][2], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step < 13 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
			end
		elseif self.d == i and self.step <= 20 then
			if self.d == 0 then
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
			else
				love.graphics.draw(self.anim[0][i][3], self.x, self.y)
				love.graphics.draw(sword[0][i][4], self.x - 64, self.y - 72)
			end
		end
	end
end