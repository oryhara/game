function loadMoveSprites(path)
	
	name = {}
	for i=0, 3 do
		name[i] = {}
		for j=1, 3 do
			name[i][j] = love.graphics.newImage(path .. i .. j .. ".png")
		end
	end
	return name
end


function loadSprites(num1, num2, num3, path)
	
	name = {}
	for i=0, num1 do
		name[i] = {}
		for j=0, num2 do
			name[i][j] = {}
			for k=1, num3 do
				name[i][j][k] = love.graphics.newImage(path .. i .. j .. k .. ".png")
			end
		end
	end
	return name
end