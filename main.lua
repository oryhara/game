require "sprites"
require "animations"
require "health"
require "character"
require "createCharacters"
require "hitbox"
require "cupid"
require "vector"
require "attack"
require "movingAttack"

--variables to set screen resolution
windowHeight = 0
windowWidth = 0

function love.load()
	love.window.setMode(1280, 800--[[, {fullscreen = true}]])
	windowHeight = love.window.getHeight()
	windowWidth = love.window.getWidth()
	
	--Load: movement sprites
	WarMMove = loadMoveSprites("Images/Char_WarM/Move_")
	WarFMove = loadMoveSprites("Images/Char_WarF/Move_")
	
	--Load: attack sprites
	WarMAtt = loadSprites(0, 3, 3, "Images/Char_WarM/Att_")
	WarFAtt = loadSprites(0, 3, 3, "Images/Char_WarF/Att_")
	
	--Load: attack effect sprites
	sword = loadSprites(1, 3, 4, "Images/Attack/")
	
	player = Character.create("player", WarFMove, WarFAtt, 620, 20, 100, 100, 5, 5, 5)
	
	characters = {}
	characters = createCharactersRand()
	math.randomseed(os.time())
end


function love.update(dt)
	
	
	if love.keyboard.isDown("u") and not player.isBusy and player.combo == 0 then
		player.isBusy = true
		player.attack:set("01")
	end
	
	if love.keyboard.isDown("i") and not player.isBusy and player.combo == 0 then
		player.isBusy = true
		player.attack:set("11")
	end
	
	if love.keyboard.isDown("n") then
		player.showStats = true
		debug.debug()
	else
		player.showStats = false
	end
	
	for i, character in pairs(characters) do
		--print ("Updating characters "..i)
		character:update(dt)
	end
	

end


function love.draw()
	--Background setup
	love.graphics.setColor(0, 215, 0, 255)
	love.graphics.rectangle("fill", 0, 465, 1280, 350)
	
	love.graphics.setColor(193, 151, 0, 255)
	love.graphics.rectangle("fill", 0, 0, 1280, 465)
	
	if player.showStats then
		player:drawStats()
	end

	order = {}
	order[player.y] = {}
	table.insert(order[player.y], player)
	for n, character in pairs(characters) do
		if (order[character.y] == nil) then
			order[character.y] = {}
		end
		table.insert(order[character.y], character)
	end
	
	for i, thisY in spairs(order) do
		--i = character.y, thisY = {characters}, 
		for j, character in pairs(thisY) do
			character:draw()
		end
	end

end

function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
        --keys[i] = character.y, t[keys[i]] = order[character.y]
            return keys[i], t[keys[i]]
        end
    end
end

