

Character = {}
Character.__index = Character

function aggressive(ai, dt)
	if not (ai.targetCharacter == nil) and not ai.targetCharacter.isDead then
		decision = math.random(2)
		if not (ai.timer > 0) then
			if (ai.attTarget.magnitude < 80) then
				if decision == 1 then
					ai.isBusy = true
					ai.attack:set("11")
					ai.timer = 40
				elseif decision == 2 then
					ai.isBusy = true
					ai.attack:set("01")
					ai.timer = 40
				end
			elseif (ai.attTarget.magnitude < 200) then
				ai.isBusy = true
				ai.attack:set("02")
				ai.timer = 60
			end
		end
	end
end

function fake(ai, dt)
	if not (ai.timer > 0) then
		ai.isBusy = true
		ai.attack:set("01")
		ai.timer = 60
	end
end

function defensive(ai, dt)
	if not (ai.targetCharacter == nil) and not ai.targetCharacter.isDead then
		decision = math.random(2)
		if not (ai.timer > 0) then
			if decision == 1 then
				if (ai.attTarget.magnitude < 80) then
					ai.isBusy = true
					ai.attack:set("03")
					ai.timer = 120
				end
			elseif decision == 2 then
				if (ai.attTarget.magnitude < 80) then
					ai.isBusy = true
					ai.attack:set("01")
					ai.timer = 70
				end
			end
		end
	end
end

function ranged(ai, dt)
	print (ai.attTarget.magnitude)
	if not (ai.targetCharacter == nil) and not ai.targetCharacter.isDead then
		if (ai.attTarget.magnitude < 350) then
			if (ai.speed > 0) then
				ai.speed = 0 - ai.speed
			end
			if not (ai.timer > 0) then
				ai.isBusy = true
				ai.attack:set("21")
				ai.timer = 120
			end
		end
		if (ai.attTarget.magnitude > 350) and (ai.speed < 0) then
			ai.speed = 0 - ai.speed
		end
	end
end

function test(ai, dt)
	if not (ai.timer > 0) then
		if not (ai.targetCharacter == nil) and not ai.targetCharacter.isDead then
			if (ai.target.magnitude < 80) then
				ai.isBusy = true
				ai.attack:set("12")
				ai.timer = 80
			elseif (ai.target.magnitude < 180) then
				ai.isBusy = true
				ai.attack:set("02")
				ai.timer = 80
			elseif (ai.target.magnitude < 400) and not ai.movingAttack.live then
				ai.isBusy = true
				ai.attack:set("21")
				ai.timer = 80
			end
		end
	end
end
function aiUpdate(ai, dt)
				
			--print (ai.target.magnitude)
	if (ai.targetCharacter ~= nil ) then
		ai.target:setX(ai.targetCharacter.x - ai.x)
		ai.target:setY(ai.targetCharacter.y - ai.y)
		
		ai.attTarget:setX(ai.targetCharacter.x - ai.x)
		ai.attTarget:setY(ai.targetCharacter.y - ai.y)
	
		if (math.abs(ai.target.y) > math.abs(ai.target.x)) then
			if ai.target.y > 0 then
				ai.d = 2
			elseif ai.target.y < 0 then
				ai.d = 0
			end
		elseif (math.abs(ai.target.y) < math.abs(ai.target.x)) then
			if (ai.target.x > 0) then
				ai.d = 1
			elseif (ai.target.x < 0) then
				ai.d = 3
			end
		end
	
		if (ai.timer > 0) and not (ai.hitstun > 0) then
			ai.timer = ai.timer - 1
		end
		if not (ai.hitstun > 0) then
			if ai.personality == "aggressive" then
				if (ai.target.magnitude < 80) then
					ai.target:setX(0)
					ai.target:setY(0)
				end
				aggressive(ai, dt)
			elseif ai.personality == "test" then
				if (ai.target.magnitude < 80) then
					ai.target:setX(0)
					ai.target:setY(0)
				end
				test(ai, dt)
			elseif ai.personality == "defensive" then
				if (ai.target.magnitude < 80) then
					ai.target:setX(0)
					ai.target:setY(0)
				end
				defensive(ai, dt)
			elseif ai.personality == "fake" then
				if (ai.target.magnitude < 80) then
					ai.target:setX(0)
					ai.target:setY(0)
				end
				fake(ai, dt)
			elseif ai.personality == "ranged" then
				ranged(ai, dt)
			end
			if not (ai.target.magnitude == 0) and (ai.target.magnitude < 2) then
				ai.target:setX(math.random(-1, 1))
				ai.target:setY(math.random(-1, 1))
			end
		end

	else
		ai.target:setX(0)
		ai.target:setY(0)
	end
	
	if not (ai.hitstun > 0) and (ai.step < 60) then
		ai.step = ai.step + 1
	else
		ai.step = 0
	end
end

function enemyUpdate(enemy, dt)
	--print("enemy update")
	aiUpdate(enemy, dt)
end

function partyUpdate(party, dt)
    --print("party update")
    if (party.targetCharacter ~= nil) and (party.targetCharacter.isDead) then
	    party:setTargetCharacter(nil)
	    --print ("nullifying target")
    end
	
	for i, character in pairs(characters) do
		if (character.person == "enemy") and not (character.isDead) then
			if (party.targetCharacter == nil) then
				--print ("Target was nil, setting to "..i)
				party:setTargetCharacter(character)
			end
			testVector = Vector.create(character.x - party.x, character.y - party.y)
			targetVector = Vector.create(party.targetCharacter.x - party.x, party.targetCharacter.y - party.y)
			if (testVector.magnitude < targetVector.magnitude) then
				party:setTargetCharacter(character)
			end
		end
	end
	aiUpdate(party, dt)
end

function playerUpdate(playerChar, dt)
	if love.keyboard.isDown("w") then
		playerChar.target:setY(-1)
		playerChar.d = 0
	elseif love.keyboard.isDown("s") then
		playerChar.target:setY(1)
		playerChar.d = 2
	else
		playerChar.target:setY(0)
	end

	if love.keyboard.isDown("a") then
		playerChar.target:setX(-1)
		playerChar.d = 3
	elseif love.keyboard.isDown("d") then
		playerChar.target:setX(1)
		playerChar.d = 1
	else
		playerChar.target:setX(0)
	end
	
	if love.keyboard.isDown("w", "a", "s", "d") then
		playerChar.step = (playerChar.step + 1) % 60
	else
		playerChar.step = 12
	end
end

function Character.create(person, move, attack, x, y, initialHP, speed, offense, defense, balance, personality)
	local unit = {}
	setmetatable(unit, Character)
	unit.speed = speed
	unit.offense = offense
	unit.defense = defense
	unit.balance = balance
	unit.showStats = false
	unit.move = move
	unit.state = 2
	unit.d = 2
	unit.direction = 0
	unit.step = 10
	unit.attackStep = 0
	unit.hitstun = 0
	unit.timer = 0
	unit.poison = 0
	unit.paralyze = false
	unit.attack = Attack.create(attack, unit, offense)
	unit.movingAttack = MovingAttack.create(unit)
	unit.xSpeed = 0
	unit.ySpeed = 0
	unit.isBusy = false
	unit.combo = 0
	unit.x = x
	unit.y = y
	unit.drawOrder = 0
	unit.target = Vector.create(0, 0)
	unit.attTarget = Vector.create(0, 0)
	unit.person = person
	unit.health = Health.create(initialHP, x, y, unit)
	unit.hurtboxX = x + 8
	unit.hurtboxY = y
	unit.hurtboxX2 = x + 56
	unit.hurtboxY2 = y + 68
	unit.isDead = false
	unit.targetCharacter = nil
	if (person == "enemy") then
		unit.updateControl = enemyUpdate
	elseif (person == "party") then
	    unit.updateControl = partyUpdate
	else
		unit.updateControl = playerUpdate
	end
	unit.personality = personality
	return unit
end

function Character:setTargetCharacter(target)
	debug.debug()
	self.targetCharacter = target
end

function Character:update(dt)

	if self.health.current <= 0 then
		self.isDead = true
	end

	if not self.isDead then
		--print (self.x .. "  " .. self.y)
		self.health:setCoordinates(self.x, self.y)
	
		self.hurtboxX = self.x + 8
		self.hurtboxY = self.y
		self.hurtboxX2 = self.x + 56
		self.hurtboxY2 = self.y + 68
		
		self.attack:update(self.x, self.y, self.d, self.attTarget)
		
		if (self.hitstun > 0) then
			self.hitstun = self.hitstun - 1
			self.isBusy = true
		elseif (self.attack:check() == "none") then
			self.isBusy = false
			self.paralyze = false
		end
		
		if self.poison > 0 then
			if (self.poison % 100 == 0) then
				self.health:change(-5)
			end
			self.poison = self.poison - 1
		end
		self.updateControl(self, dt)

		if not self.isBusy then
		
		--add "decision" function
			
		--extract closest enemy function
		
		
			if (self.target.magnitude ~= 0) then
				self.xSpeed = self.speed * self.target:xUnit()
				self.ySpeed = self.speed * self.target:yUnit()
			else
				self.xSpeed = 0
				self.ySpeed = 0
			end
		end
		self:updatePosition(dt)
	end

	self.movingAttack:update()
end

function Character:updatePosition(dt)
				
				
	--update direction function
	--print out the speed
	--print(self.xSpeed .. " " .. self.ySpeed)
	if not self.isBusy then
		self.y = self.y + self.ySpeed*dt
		self.x = self.x + self.xSpeed*dt
	else
		self.y = self.y + self.attack.ySpeed*dt
		self.x = self.x + self.attack.xSpeed*dt
	end
	self.y = math.max(self.y, 20)
	self.y = math.min(self.y, 720)
	self.x = math.max(self.x, 0)
	self.x = math.min(self.x, 1280 - 56)
end

function Character:draw()
	self.movingAttack:draw()

	if not self.isDead then
		self.health:drawBar()
		if self.hitstun > 0 then
			love.graphics.setColor(255, 200, 200)
		elseif self.paralyze == true then
			love.graphics.setColor(255, 217, 100)
		elseif self.poison > 0 then
			love.graphics.setColor(255, 100, 100)
		else
			love.graphics.setColor(255, 255, 255)
		end
		if (self.attack:check() ~= "none") then
			self.attack:animate() 
		else
			self:animMove()
		end
	end
end

function Character:animMove()
	--walk cycle
	for i=0, 3 do
		if self.d == i and self.step < 15 then
			love.graphics.draw(self.move[i][2], self.x, self.y)
		elseif self.d == i and self.step < 30 then
			love.graphics.draw(self.move[i][1], self.x, self.y)
		elseif self.d == i and self.step < 45 then
			love.graphics.draw(self.move[i][2], self.x, self.y)
		elseif self.d == i and self.step <= 60 then
			love.graphics.draw(self.move[i][3], self.x, self.y)
		end
	end
end

function Character:drawStats()
	love.graphics.setColor(0, 0, 0, 100)
	love.graphics.rectangle("fill", 0, 0, 140, 160)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print("Health: " .. self.health.current .. "/" .. self.health.initial, 20, 20)
	love.graphics.print("Offense: " .. self.offense, 20, 40)
	love.graphics.print("Defense: " .. self.defense, 20, 60)
	love.graphics.print("Balance: " .. self.balance, 20, 80)
	love.graphics.print("Speed: " .. self.speed, 20, 100)
end