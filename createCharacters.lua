
function createCharacters0()
	characters = {}
	characters[0] = player
	characters[1] = Character.create("enemy", WarMMove, WarMAtt, 450, 250, 100, 90, 2, 2, 2, "aggressive")
	return characters
end


function createCharacters1()
	characters = {}
	characters[0] = player
	characters[1] = Character.create("enemy", WarMMove, WarMAtt, 450, 250, 100, 90, 2, 2, 2, "defensive")
	characters[2] = Character.create("party", WarMMove, WarMAtt, 800, 350, 100, 90, 2, 2, 2, "aggressive")
	characters[3] = Character.create("enemy", WarMMove, WarMAtt, 150, 250, 100, 90, 2, 2, 2, "defensive")
	return characters
end


function createCharacters2()
	characters = {}
	characters[0] = player
	characters[1] = Character.create("enemy", WarMMove, WarMAtt, 650, 250, 100, 90, 2, 2, 2, "defensive")
	characters[2] = Character.create("party", WarMMove, WarMAtt, 800, 250, 100, 90, 2, 2, 2, "defensive")
	characters[3] = Character.create("enemy", WarMMove, WarMAtt, 650, 150, 100, 90, 2, 2, 2, "aggressive")
	characters[4] = Character.create("party", WarMMove, WarMAtt, 800, 150, 100, 90, 2, 2, 2, "aggressive")
	characters[5] = Character.create("enemy", WarMMove, WarMAtt, 650, 750, 100, 90, 2, 2, 2, "ranged")
	characters[6] = Character.create("party", WarMMove, WarMAtt, 800, 700, 100, 90, 2, 2, 2, "ranged")
	return characters
end


function createCharacters3()
	characters = {}
	characters[0] = player
	characters[1] = Character.create("enemy", WarMMove, WarMAtt, 800, 450, 100, 80, 2, 2, 2, "aggressive")
	characters[2] = Character.create("enemy", WarFMove, WarFAtt, 700, 390, 100, 80, 2, 2, 2, "ranged")
	characters[3] = Character.create("enemy", WarFMove, WarFAtt, 800, 400, 100, 90, 2, 2, 2, "defensive")
	characters[4] = Character.create("party", WarMMove, WarMAtt, 200, 500, 100, 90, 2, 2, 2, "aggressive")
	characters[5] = Character.create("party", WarFMove, WarFAtt, 100, 100, 100, 90, 2, 2, 2, "ranged")
	characters[6] = Character.create("party", WarMMove, WarMAtt, 200, 400, 100, 90, 2, 2, 2, "defensive")
	return characters
end


function createCharactersRand()
	math.randomseed(os.time())
	characters = {}
	characters[0] = player
	characters[1] = Character.create("enemy", WarMMove, WarMAtt, math.random(100, 1100), math.random(100, 550), 100, 80, 2, 2, 2, "aggressive")
	characters[2] = Character.create("enemy", WarFMove, WarFAtt, math.random(100, 1100), math.random(100, 550), 100, 80, 2, 2, 2, "ranged")
	characters[3] = Character.create("enemy", WarFMove, WarFAtt, math.random(100, 1100), math.random(100, 550), 100, 90, 2, 2, 2, "defensive")
	characters[4] = Character.create("enemy", WarFMove, WarFAtt, math.random(100, 1100), math.random(100, 550), 100, 90, 2, 2, 2, "defensive")
	characters[5] = Character.create("party", WarMMove, WarMAtt, math.random(100, 1100), math.random(100, 550), 100, 90, 2, 2, 2, "aggressive")
	characters[6] = Character.create("party", WarFMove, WarFAtt, math.random(100, 1100), math.random(100, 550), 100, 90, 2, 2, 2, "ranged")
	characters[7] = Character.create("party", WarMMove, WarMAtt, math.random(100, 1100), math.random(100, 550), 100, 90, 2, 2, 2, "defensive")
	return characters
end