

Vector = {}
Vector.__index = Vector

function Vector.create(xDirection, yDirection)
	local vect = {}
	setmetatable(vect, Vector)
	vect.magnitude = math.pow(math.pow(xDirection, 2) + math.pow(yDirection, 2), .5)
	vect.x = xDirection
	vect.y = yDirection
	return vect
end

function Vector:xUnit()
	return self.x / self.magnitude
end

function Vector:yUnit()
	return self.y / self.magnitude
end

function Vector:setX(newX)
	self.x = newX
	self.magnitude = math.pow(math.pow(self.x, 2) + math.pow(self.y, 2), .5)
end

function Vector:setY(newY)
	self.y = newY
	self.magnitude = math.pow(math.pow(self.x, 2) + math.pow(self.y, 2), .5)
end

function Vector:magnitude()
	return self.magnitude
end