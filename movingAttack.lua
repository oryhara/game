

MovingAttack = {}
MovingAttack.__index = MovingAttack

function MovingAttack.create(person)
	local shot = {}
	setmetatable(shot, MovingAttack)
	shot.live = false
	shot.step = 0
	shot.x = 0
	shot.y = 0
	shot.xSpeed = 0
	shot.ySpeed = 0
	shot.d = 0
	shot.offense = person.offense
	shot.person = person
	shot.target = nil
	shot.hitbox = Hitbox.create(5, 5, 50, 50, 5, person)
	return shot
end

function MovingAttack:initialize(effect)
	self.x = self.person.x
	self.y = self.person.y
	self.xSpeed = 5 * self.person.target:xUnit()
	self.ySpeed = 5 * self.person.target:yUnit()
	self.d = self.person.d
	self.hitbox:standard0(effect)
	self.live = true
	self.step = 100

end

function MovingAttack:update()
	if self.live then
		if self.person.person == "party" then
			print (self.person.x + 64)
			print (self.hitbox.x)
			print (self.hitbox.x2)
		end
		if self.step > 0 then
			self.x = self.x + self.xSpeed
			self.y = self.y + self.ySpeed
			self.step = self.step - 1
			if self.d == 0 then
				self.hitbox:setPosition(self.x + 36, self.y - 44, self.x + 52, self.y + 4)
			elseif self.d == 1 then
				self.hitbox:setPosition(self.x + 64, self.y + 36, self.x + 106, self.y + 52)
			elseif self.d == 2 then
				self.hitbox:setPosition(self.x + 12, self.y + 72, self.x + 28, self.y + 112)
			elseif self.d == 3 then
				self.hitbox:setPosition(self.x - 44, self.y + 36, self.x + 4, self.y + 52)
			end
		else
			self.live = false
		end
		self:checkDamage(10, effect)
	end
end

function MovingAttack:dealDamage(target, stun, effect)
	if self.hitbox:check(target.hurtboxX, target.hurtboxY, target.hurtboxX2, target.hurtboxY2) then
		target.health:change(-self.hitbox.damage - self.offense + target.defense)
		target.hitstun = stun - target.balance
		target:setTargetCharacter(self.person)
		
		if (effect == "paralyze") then
			target.hitstun = target.hitstun + 300
			target.paralyze = true
		end
		if (effect == "poison") then
			target.poison = 1000
		end
		if (effect == "knockback") then
			
		end
		self.live = false
		self.hitbox.hit = false
	end
end

function MovingAttack:checkDamage(stun)
	if (self.person.person == "enemy") then
		for i in pairs(characters) do
			if ((characters[i].person == "party") or (characters[i].person == "player")) and not (characters[i].isDead) then
				self:dealDamage(characters[i], stun, effect)
			end
		end
		
	elseif (self.person.person == "party") or (self.person.person == player) then
		for i in pairs(characters) do
			if (characters[i].person == "enemy") and not (characters[i].isDead) then
				self:dealDamage(characters[i], stun, effect)
			end
		end
	end
end

function MovingAttack:draw()
	if self.live then
		for i=0, 3 do
			if self.d == i then
				love.graphics.setColor(255, 255, 255)
				love.graphics.draw(sword[0][i][3], self.x - 64, self.y - 72)
			end
		end
	end
end