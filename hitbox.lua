

Hitbox = {}
Hitbox.__index = Hitbox

function Hitbox.create(x, y, x2, y2, damage, person)
	local box = {}
	setmetatable(box, Hitbox)
	box.x = x
	box.y = y
	box.x2 = x2
	box.y2 = y2
	box.damage = damage
	box.hit = false
	box.person = person
	return box
end

function Hitbox:setPosition(newX, newY, newX2, newY2)
	self.x = newX
	self.x2 = newX2
	self.y = newY
	self.y2 = newY2
end

function Hitbox:standard0()
	if self.person.d == 0 then
		self:setPosition(self.person.x + 36, self.person.y - 44, self.person.x + 52, self.person.y + 4)
	elseif self.person.d == 1 then
		self:setPosition(self.person.x + 64, self.person.y + 36, self.person.x + 106, self.person.y + 52)
	elseif self.person.d == 2 then
		self:setPosition(self.person.x + 12, self.person.y + 72, self.person.x + 28, self.person.y + 112)
	elseif self.person.d == 3 then
		self:setPosition(self.person.x - 44, self.person.y + 36, self.person.x + 4, self.person.y + 52)
	end
end

function Hitbox:standard1()
	if self.person.d == 0 then
		self:setPosition(self.person.x + 16, self.person.y - 24, self.person.x + 76, self.person.y + 12)
	elseif self.person.d == 1 then
		self:setPosition(self.person.x + 52, self.person.y + 12, self.person.x + 88, self.person.y + 72)
	elseif self.person.d == 2 then
		self:setPosition(self.person.x - 12, self.person.y + 64, self.person.x + 48, self.person.y + 100)
	elseif self.person.d == 3 then
		self:setPosition(self.person.x - 24, self.person.y + 12, self.person.x + 12, self.person.y + 72)
	end
end

function Hitbox:check(hurtboxX, hurtboxY, hurtboxX2, hurtboxY2)
	for i=self.x, self.x2 do
		for j=self.y, self.y2 do
			if i >= hurtboxX and i <= hurtboxX2 and j >= hurtboxY and j <= hurtboxY2 then
				self.hit = true
			end
		end
	end
	
	return self.hit
end

function Hitbox:draw()
	love.graphics.setColor(255, 0, 0)
	love.graphics.rectangle("fill", self.x, self.y, self.x2 - self.x, self.y2 - self.y)
end