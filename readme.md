# Game
## Controls:
* move up: **w**
* move down: **s**
* move left: **a**
* move right: **d**


* attack 1: **u**
* attack 2: **i**
* view character stats: **n**
* end game: **cmd + q**

## Character types
Different health bar colors indicate different units:
* green: **player**
* blue: **ally**
* orange: **enemy**